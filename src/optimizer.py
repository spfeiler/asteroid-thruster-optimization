import sys;
sys.path.insert(1, "./res");


from thruster_configuration import configuration,file_name;
import scipy;
import numpy as np;
import random;
import time;
import matplotlib.pyplot as plt;


#
#  Parameters
#

# Number of points we sample to calculate the error
direction_sample_points = 30;

# Max Force any thruster can produce
max_thrust_in_newton = 3;


# Target function
def Target(phi, theta):
	return max_thrust_in_newton * 20 + np.sin(theta) * 10 * max_thrust_in_newton;

# Acceptable error in any direction
acceptable_error = 5;




#
# Genetic Algorithm Parameters

# Iteration limit
MAX_ITERATIONS = 400;

# Size of the population
population_max = 150;

# Top % are considered elite
elite_percent = 0.10;

# 
children_percent = 0.45;

#  Determines the percent of population that will be the result of mutation
mutation_percent = 0.4;
radiation = 0.02;

# Determines how likely a bitflip will occur
def mutation_chance():
	return random_generation_chance * radiation;

# Random Generation Chance
random_generation_chance = 3/float(20);


assert(children_percent + elite_percent + mutation_percent <= 1);

#
#  Setup
#

PROBABILITY_PRECISION = 100000;


random.seed(time.time());
pi = np.pi;
sin = np.sin;
cos = np.cos;
arccos = np.arccos;

goldenRatio = (1 + 5**0.5)/2;







def maximize(bounds, mat):
	b_eq = np.zeros(6, dtype=float);
	cost = np.zeros(len(bounds), dtype=float);
	cost[len(bounds) - 1] = -1;
	res = scipy.optimize.linprog(c = cost, A_eq = mat, b_eq = b_eq, bounds = bounds, method='highs');
	if res.success:
		val = res.fun;
		if abs(res.fun) < 0.000001: val = 0;
		return abs(val);
	else:
		print(f"SciPy had error {res.status}\n\n");
		return 0;


def error_in_direction(phi, theta, mat, bounds):
	x = cos(phi) * sin(theta);
	y = sin(phi) * sin(theta);
	z = cos(theta);
	N = len(mat[0]) - 1;
	mat[0][N] = - x;
	mat[1][N] = - y;
	mat[2][N] = - z;
	mat[3][N] = 0;
	mat[4][N] = 0;
	mat[5][N] = 0;
	mag = maximize(bounds, mat);
	return float(mag) - Target(phi, theta), mag;

def create_bounds(pop):
	bounds = [];
	for i, present in enumerate(pop):
		ub = max_thrust_in_newton if present else 0;
		bounds.append((0, ub));
	bounds.append((0, len(pop) * max_thrust_in_newton));
	return bounds;

evaluation_history = [];

def calc_error(pop, cem):
	bounds = create_bounds(pop);
	error_sum = float(0.0);
	#print(f"{pop}");
	within_acceptable_error = True;
	for i in range(0, direction_sample_points):
		phi = 2 *pi * i / goldenRatio;
		theta = arccos(1 - 2*(i+0.5)/direction_sample_points);
		error, mag = error_in_direction(phi, theta, cem, bounds);
		error = abs(error);
		within_acceptable_error &= error < acceptable_error
		error_sum += error;
	evaluation_history.append((error_sum, random_generation_chance));
	return error_sum, within_acceptable_error;
	

def starting_population(genome_length):
	population = [];
	for i in range(0, population_max):
		pop = [];
		for gene in range(0, genome_length):
			v = random.randrange(0,PROBABILITY_PRECISION) <  random_generation_chance * PROBABILITY_PRECISION;
			pop.append(v);
		population.append(pop);
	return population;

def evaluate(p, cem):
	best = float("inf");
	worst = float(0);
	evaluation = [];
	for i, pop in enumerate(p):
		changed = False;
		fitness, acceptable = calc_error(pop, cem);
		if fitness < best:
			best = fitness;
			changed = True;
		if fitness > worst: 
			worst = fitness;
			changed = True;
		evaluation.append((fitness, pop, acceptable));
		if changed:
			sys.stdout.write(f"\tbest = {best}, worst = {worst} {i+1}/{len(p)}						 \r");
			sys.stdout.flush();

	sys.stdout.write("\n");
	evaluation.sort();
	return evaluation;

def create_controll_effectiveness_matrix(config):
	N = len(config);
	cem = np.zeros((6, N + 1), dtype=float);
	for i, t in enumerate(config):
		#print(f"{t}");
		d = t["direction"];
		p = t["position"];

		cem[0][i] = d[0];
		cem[1][i] = d[1];
		cem[2][i] = d[2];

		c = np.cross(p, d); # Torque = (position cross force_direction) * magnitude
		cem[3][i] = c[0];
		cem[4][i] = c[1];
		cem[5][i] = c[2];
	return cem;

if __name__ == "__main__":
	genome_length = len(configuration);
	POPULATION_COUNT = population_max;
	N_ELITE = int(elite_percent	   * POPULATION_COUNT);
	N_CROSS = int(children_percent * POPULATION_COUNT);
	N_MUT   = int(mutation_percent * POPULATION_COUNT);
	N_GEN   = population_max - (N_ELITE + N_CROSS + N_MUT);

	P_ELITE = float(N_ELITE)/POPULATION_COUNT;
	P_CROSS = float(N_CROSS)/POPULATION_COUNT;
	P_MUT   = float(N_MUT  )/POPULATION_COUNT;
	P_GEN   = float(N_GEN  )/POPULATION_COUNT;
	P_RAD   = radiation;

	print(f"Genomes are {genome_length} bits long");
	print(f"Population is {population_max} individuals");
	print(f"Elite will be top {P_ELITE * 100}%  N = {N_ELITE}");
	print(f"Children will be {P_CROSS * 100}% of new population N = {N_CROSS}");
	print(f"Mutations will be {P_MUT * 100}% of new population  N = {N_MUT}");
	print(f"Mutations will flip a bit with a chance of {mutation_chance() * 100}% -> {int(genome_length * mutation_chance())} bit flips");
	random_percent = (1 - mutation_percent - elite_percent - children_percent);
	print(f"Random genomes are {P_GEN * 100}% of the new population  N = {N_GEN}");
	print(f"Random genomes have {random_generation_chance * 100}% chance that a bit is set");
	print(f"We will sample {direction_sample_points} directions");
	#
	# Create Controll Effectiveness Matrix CEM
	#
	cem = create_controll_effectiveness_matrix(configuration);

	#
	#  Create Initial Population
	#
	population = starting_population(genome_length);
	for it in range(0,MAX_ITERATIONS):
		print(f"iteration {it + 1}");
		evaluation = evaluate(population, cem);
		length = len(evaluation);
		if evaluation[0][2]:
			print("We have found an acceptable solution\n");
			break;

		new_population = [];
		parents = [];
		runner = 0;
		while len(new_population) <= int(elite_percent * len(evaluation)) and runner < len(evaluation):
			p = evaluation[runner][1];
			runner += 1;
			if p not in new_population:
				parents.append(p);
				new_population.append(p);
				sys.stdout.write(f"\tPicked Parent {len(parents)}/{int(elite_percent * len(evaluation))}				 \r");
				sys.stdout.flush();
		if runner >= len(evaluation): runner -= 1;
		print(f"\tBest Parent = {evaluation[0][0]} Worst Parent = {evaluation[runner][0]}");
		before = random_generation_chance;
		ammount_of_active_thrusters_in_best = parents[0].count(True);
		random_generation_chance = float(ammount_of_active_thrusters_in_best)/genome_length;
		parents_length = len(parents);
		
		#
		#  Crossover
		#
		for i in range(0, int(len(population) * children_percent)):
			parent_1_idx = random.randrange(0, parents_length);
			parent_2_idx = random.randrange(0, parents_length);
			p1 = parents[parent_1_idx];
			p2 = parents[parent_2_idx];
			assert(len(p1)==len(p2));
			child = [];
			for i in range(0, len(p1)):
				bit = p1[i];
				if random.randrange(0,2) == 1: bit = p2[i];
				child.append(bit);
			assert(len(child) == len(p2));
			new_population.append(child);

		#
		#  Mutation
		#
		count_before_mutation = len(new_population);
		for i in range(0, int(population_max * mutation_percent)):
			target_idx = random.randrange(0, count_before_mutation);
			new_pop = [];
			for p in population[target_idx]:
				bit = p;
				if random.randrange(0, PROBABILITY_PRECISION) < int(mutation_chance() * PROBABILITY_PRECISION):
					bit = not bit;
				new_pop.append(bit);

			new_population.append(new_pop);


		#
		#  Fill remaining population with random 
		#
		cnt = len(new_population);
		for i in range(cnt, population_max):
			pop = [];
			for gene in range(0, genome_length):
				v = random.randrange(0,PROBABILITY_PRECISION) < random_generation_chance * PROBABILITY_PRECISION;
				pop.append(v);
			new_population.append(pop);
		assert(len(population) == len(new_population));
			
		population = new_population;

	evaluation = evaluate(population, cem);
	best = evaluation[0][1];
	print(f"Finished with best = {evaluation[0][0]}");
	if not evaluation[0][2]:
		print(f"It does not satisfy the acceptable_error for all directions!");
	with open(f"{file_name}_optimized.csv", "w") as res:
		print("outputting csv for blender");
		for i, p in enumerate(best):
			if p:
				po = configuration[i]["position"];
				di = configuration[i]["direction"];
				res.write(f"{po[0]}, {po[1]}, {po[2]}, {di[0]}, {di[1]}, {di[2]}\n");
		res.close();

	with open("res/optimization_result.py", "w") as res:
		print("outputting optimization_result.py");
		res.write("configuration = [\n");
		for i, p in enumerate(best):
			if p:
				res.write(f"\t{configuration[i]},\n");
		res.write("];");
		res.write(f"file_name = \"{file_name}\";\n");
		res.flush();

	with open("res/optimization_history.py", "w") as his:
		print("outputting optimization_history.py");
		his.write("evaluation_history = [\n");
		for error, ratio in evaluation_history:
			his.write(f"\t{error},\n");
		his.write("];\n");
		his.write("p_ratio = [\n");
		for error, ratio in evaluation_history:
			his.write(f"\t{ratio},\n");
		his.write("];\n");

	with open("res/parameters.tex", "w") as params:
		print("outputting parameters.tex");
		params.write(f"Genome Length = {genome_length}\\\\\n");
		params.write("\\begin{tabular}{|l r|c|}\n");
		params.write("\\hline\n");
		params.write("Parameter Name && Value\\\\\n");
		params.write("\\hline\n");
		params.write(f"Population && {population_max}\\\\\n");
		params.write("\\hline\n");
		params.write("Elite & $p_{elite}$& "+f"{P_ELITE * 100:4.2f} \% ({N_ELITE})\\\\\n");
		params.write("\\hline\n");
		params.write("Crossover & $p_{crossover}$ & "+f"{P_CROSS * 100:4.2f} \% ({N_CROSS})\\\\\n");
		params.write("\\hline\n");
		params.write("Mutation & $p_{mutation}$& "+f"{P_MUT * 100:4.2f} \% ({N_MUT})\\\\\n");
		params.write("\\hline\n");
		params.write("Random Generation & $p_{generation}$& "+f"{P_GEN * 100:4.2f} \% ({N_GEN})\\\\\n");
		params.write("\\hline\n");
		params.write("Radiation & $p_{radiation}$& "+f"{P_RAD * 100:4.2f} \% \\\\\n");
		params.write("\\hline\n");
		params.write("\\end{tabular}\n");



def evaluate_grid(config, x_resolution, y_resolution):
	bounds = create_bounds([True] * len(config));
	mat = create_controll_effectiveness_matrix(config);
	errors = [];
	mag = [];
	target = [];
	for yi in range(0, y_resolution):
		y = (float(180)/y_resolution * yi) * pi / 180;
		errors_y = [];
		mag_y = [];
		target_y = [];
		for xi in range(0, x_resolution):
			x = (float(360)/x_resolution * xi) * pi / 180;
			error, magnitude = error_in_direction(x + pi/2, y, mat = mat, bounds = bounds);
			errors_y.append(error);
			mag_y.append(magnitude);
			target_y.append(Target(x, y));
		errors.append(errors_y);
		mag.append(mag_y);
		target.append(target_y);
	return errors, (0, 360), (0, 180), mag, target;
