import sys;
sys.path.insert(1, "./res");

from optimization_result import configuration,file_name;
from optimization_history import evaluation_history, p_ratio;
from optimizer import evaluate_grid;
import numpy as np;
import matplotlib.pyplot as plt;
import matplotlib as mat;
import png;



plt.style.use('_mpl-gallery-nogrid');

resolution = 32;


mat.rcParams.update({'figure.autolayout': True})

error, x_extent, y_extent, magnitude, target = evaluate_grid(configuration, resolution * 2, resolution);



#fig, axis = plt.subplots(3, 2, figsize=(10,7));
plt.figure(figsize=(10,5));
min_error = np.min(error);
max_error = np.max(error);
error_abs  = [[abs(val)                                for val in row] for row in error];
error_norm = [[(val-min_error)/(max_error - min_error) for val in row] for row in error];
error_int  = [[int((v-np.min(error_abs))/(np.max(error_abs) - np.min(error_abs)) * 255)                                 for v   in row] for row in error_abs];


min_magnitude = np.min(magnitude);
max_magnitude = np.max(magnitude);
magnitude_abs  = [[abs(val)                                            for val in row] for row in magnitude];
magnitude_norm = [[(val-min_magnitude)/(max_magnitude - min_magnitude) for val in row] for row in magnitude];
magnitude_int  = [[int(v * 255)                                             for v   in row] for row in magnitude_norm];

target_int = [[int((v - np.min(target))/(np.max(target) - np.min(target)) * 255) for v in row] for row in target];



png.from_array(error_int, mode = "L").save(f"{file_name}_error.png");
png.from_array(magnitude_int, mode = "L").save(f"{file_name}_mag.png");
png.from_array(target_int, mode = "L").save(f"{file_name}_target.png");

plt.tight_layout();

plt.imshow(magnitude,      cmap="gray", extent =(x_extent[0], x_extent[1], y_extent[1], y_extent[0]), interpolation="bicubic",                                 origin="lower")
plt.suptitle(r"$Force(C, \phi, \theta)$");
plt.xlabel(r"$\phi$");
plt.ylabel(r"$\theta$");
cb = plt.colorbar(format="%4.2f N");
plt.savefig(f"{file_name}_plot_magnitude.png");
cb.remove();

plt.imshow(target,  cmap="gray",    extent =(x_extent[0], x_extent[1], y_extent[1], y_extent[0]), interpolation="bicubic",                                 origin="lower");
plt.suptitle(r"$Target(\phi, \theta)$");
plt.xlabel(r"$\phi$");
plt.ylabel(r"$\theta$");
cb = plt.colorbar(format="%4.2f N");
plt.savefig(f"{file_name}_plot_target.png");
cb.remove();

plt.imshow(error,      cmap="seismic", extent =(x_extent[0], x_extent[1], y_extent[1], y_extent[0]), interpolation="bicubic", norm=mat.colors.CenteredNorm(), origin="lower");
plt.suptitle(r"$Error(C, \phi, \theta)$");
plt.xlabel(r"$\phi$");
plt.ylabel(r"$\theta$");
cb = plt.colorbar(format="%4.2f N");
plt.savefig(f"{file_name}_plot_error.png");
cb.remove();

plt.imshow(error_abs,  cmap="gray",    extent =(x_extent[0], x_extent[1], y_extent[1], y_extent[0]), interpolation="bicubic",                                 origin="lower");
plt.suptitle(r"$|Error(C, \phi, \theta)|$");
plt.xlabel(r"$\phi$");
plt.ylabel(r"$\theta$");
cb = plt.colorbar(format="%4.2f N");
plt.savefig(f"{file_name}_error_abs.png");
cb.remove();


fig1, axis1 = plt.subplots(1, 1, figsize=(10,7));
err = axis1.plot(evaluation_history, "x", label="error(c)", c="g");
axis1.set_xlabel("# Evaluation of error(c)");
axis1.set_ylabel("log(error(c))");
axis1.set_yscale("log");
axis1.set_title(r"Error and $p_{ratio}$ history");

axis2 = axis1.twinx();
ratio = axis2.plot(p_ratio, label=r"$p_{ratio}$", c="r");
axis2.set_ylabel(r"$p_{ratio}$");
fig1.legend();
plt.savefig(f"{file_name}_history.png");

#plt.show();

