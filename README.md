# OME Asteroid Thruster Optimization
Optimizing the number and placement of Thruster on Asteroids.

Asteroid Models from [3d-asteroids.space](https://3d-asteroids.space/)

Work in Progress Paper: ![](Paper/version1.pdf)

## Before:
![](./Paper/media/Optimization_1/All_Thrusters.png)

## After:
![](./Paper/media/Optimization_1/Selected_Thrusters.png)




