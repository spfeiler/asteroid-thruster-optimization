\babel@toc {english}{}\relax 
\contentsline {section}{\numberline {1}Motivation}{3}{}%
\contentsline {section}{\numberline {2}Problem Statement}{3}{}%
\contentsline {subsection}{\numberline {2.1}Defining a Thruster}{3}{}%
\contentsline {subsection}{\numberline {2.2}Defining the Force Profile}{3}{}%
\contentsline {subsection}{\numberline {2.3}Error function}{4}{}%
\contentsline {subsection}{\numberline {2.4}Thrust Allocation Problem}{4}{}%
\contentsline {section}{\numberline {3}Genetic Algorithm}{6}{}%
\contentsline {subsection}{\numberline {3.1}High Level Overview of the Algorithm}{6}{}%
\contentsline {subsection}{\numberline {3.2}Gene Mapping to Thrusters}{6}{}%
\contentsline {subsection}{\numberline {3.3}Stopping Criteria}{6}{}%
\contentsline {subsection}{\numberline {3.4}Crossover}{6}{}%
\contentsline {section}{\numberline {4}Results}{7}{}%
\contentsline {subsection}{\numberline {4.1}Asteroid Hygiea}{8}{}%
\contentsline {subsubsection}{\numberline {4.1.1}Genetic Algorithm Parameters}{8}{}%
\contentsline {subsubsection}{\numberline {4.1.2}Error and $p_{ratio}$ history during runtime}{9}{}%
\contentsline {subsubsection}{\numberline {4.1.3}Performance in direction $(1, \phi , \theta )$}{9}{}%
\contentsline {subsubsection}{\numberline {4.1.4}Visualization of Selected Thrusters}{10}{}%
\contentsline {subsection}{\numberline {4.2}Force Profile of the Selected Thrusters}{11}{}%
\contentsline {subsection}{\numberline {4.3}Asteroid Itokawa}{12}{}%
\contentsline {subsubsection}{\numberline {4.3.1}Genetic Algorithm Parameters}{12}{}%
\contentsline {subsubsection}{\numberline {4.3.2}Error and $p_{ratio}$ history during runtime}{13}{}%
\contentsline {subsubsection}{\numberline {4.3.3}Performance in direction $(1, \phi , \theta )$}{13}{}%
\contentsline {subsubsection}{\numberline {4.3.4}Visualization of Selected Thrusters}{14}{}%
\contentsline {subsection}{\numberline {4.4}Force Profile of the Selected Thrusters}{15}{}%
\contentsline {subsection}{\numberline {4.5}Asteroid Eros}{16}{}%
\contentsline {subsubsection}{\numberline {4.5.1}Genetic Algorithm Parameters}{16}{}%
\contentsline {subsubsection}{\numberline {4.5.2}Error and $p_{ratio}$ history during runtime}{17}{}%
\contentsline {subsubsection}{\numberline {4.5.3}Performance in direction $(1, \phi , \theta )$}{17}{}%
\contentsline {subsubsection}{\numberline {4.5.4}Visualization of Selected Thrusters}{18}{}%
\contentsline {subsection}{\numberline {4.6}Force Profile of the Selected Thrusters}{19}{}%
\contentsline {subsection}{\numberline {4.7}Asteroid Eros}{20}{}%
\contentsline {subsubsection}{\numberline {4.7.1}Genetic Algorithm Parameters}{20}{}%
\contentsline {subsubsection}{\numberline {4.7.2}Error and $p_{ratio}$ history during runtime}{21}{}%
\contentsline {subsubsection}{\numberline {4.7.3}Performance in direction $(1, \phi , \theta )$}{21}{}%
\contentsline {subsubsection}{\numberline {4.7.4}Visualization of Selected Thrusters}{22}{}%
\contentsline {subsection}{\numberline {4.8}Force Profile of the Selected Thrusters}{23}{}%
