\documentclass[11pt,a4paper]{article} %,twocolumn
\usepackage[utf8]{inputenc} 
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{makeidx}
\usepackage{graphicx}
\usepackage[left=2.00cm, right=2.00cm, top=2.00cm, bottom=2.00cm]{geometry}
\usepackage[english]{babel}
\usepackage{layout}
\usepackage[]{tikz}
\usepackage[]{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}
\usepackage[nobottomtitles*]{titlesec}
\pgfplotsset{compat=1.18, width = 8cm}

% Title Page
\title{Thruster distribution on irregular bodies}
\author{Sebastian Pfeiler 21-940-002}
\begin{document}
	
	%\layout
	\maketitle
	\newpage
	\tableofcontents
	\newpage
	
	\begin{abstract}
		
	\end{abstract}
	\section{Motivation}
	Given a set of Thrusters, it is quite easy to calculate the Thrust that those Thrusters can produce in any given direction, ie a Force Profile for every Direction in 3 Dimensions. But how can we approach the inverse of this? 
	Given the Thrust that can be produced in certain directions, how can we find the Thruster distribution on a body which matches that Force Profile. Especially with the constraint that all thrusters have to be on the 3D Surface of the body.
	In order to approach this problem we will distribute Thrusters on a 3D Surface and select Thrusters using a Genetic Algorithm to approximate the Force Profile.
	The bodies we will try to accelerate will be Asteroids from our Solar System.
%	Where to place thrusters on a body, in order to move it in a certain way, is a broadly applicable problem in Aerospace engineering as it informs the underlying flight characteristics. This work, while maybe being too general in order to give a satisfactory solution for any specific real world application, tries to explore the possibility space of Thruster placements using a Genetic Optimizer in order to provide a first estimation of a "good enough" distribution.
	\section{Problem Statement}
	Given a Body in three dimensional Space whose Center of Mass is at $\vec 0$, distribute Thrusters on the surface of the Body such that a Force Profile can be produced without introducing Torques.
	
	\subsection{Defining a Thruster}
	In this work we consider a simple Thruster that has a position $p$ and a direction $d$ as well as a maximal force $0 \leq f$ that it can produce in that direction.
	The Algorithm will take as input a set of Thrusters $T$ from which it can select Thrusters. Thrusters will be talked about in the context of this Set $T$. We refer to each thruster in the set using an index. e.g.: $p_1, d_1, f_1$ are properties of the first Thruster in the set $T$.
	\subsection{Defining the Force Profile}
	The Force Profile is a function $Target(\phi, \theta)$ which takes a direction, expressed as a point on the unit sphere in polar coordinates $(1, \phi, \theta)$ and maps it to a Force in $N$ewton, which should be produced in that direction. 
	We can see an example of a Force Profile in the figures \ref{fig:Force Profile Example}, which we will later use as an optimization Target.
	On the left, there is a Mercator Projection of the Unit Spheres, where the brightness indicates the magnitude of the force.
	On the right, we depict the Force Profile as a Surface in 3D Space where each point on the surface has polar coordinates $(Target(\phi,\theta),\phi,\theta)$.
	\begin{figure}[H]
			\begin{subfigure}[b]{0.5\textwidth}
				\centering
				\includegraphics[height=4cm]{"media/Optimization_1/thruster_configuration.py_plot_target.png"}
				\caption{Plot of the Target function, horizontal is $\phi$ longitude on unit sphere, vertical is $\theta$ latitude on unit sphere}
				\label{fig:Plof of Force Profile}
			\end{subfigure}
			\begin{subfigure}[b]{0.5\linewidth}
				\centering
				\includegraphics[height=4cm]{"media/Optimization_1/Target.png"}
				\caption{Force Profile as a 3D Surface, each points distance from the origin is the Force in the direction of the point.		}
				\label{fig:Force_Profile_Surface}
			\end{subfigure}
		\caption{Force Profile defined by the function $Target(\phi,\theta) = 60 + sin(\theta) * 30$}
		\label{fig:Force Profile Example}
	\end{figure}

	\subsection{Error function}
	In order to evaluate the Error of a set of Thrusters selected by the Genetic Algorithm and the Requirement $Target$, we have to solve the Thrust Allocation Problem in order to find the magnitude of the Force that a set of Thrusters can produce in a direction. We will then sample points on the unit sphere in order to calculate the Error of a set of Thrusters.
	The sampling will be done using a Fibonacci Sphere distribution, to avoid over-sampling at the poles and under sampling at the equator of the unit sphere \cite{Fibonacci}.
	
	\subsection{Thrust Allocation Problem}
	The Thrust Allocation Problem is generally about allocating Forces to effectors, in this case Thrusters, such that some target is being satisfied. 
	In our case we want to find the maximal Force that a given Set of Thrusters can produce in a certain direction.
	Methods like the Gauss-Seidel Pseudo Inverse or the Iterative Pseudo Inverse are computationally cheap but do not guarantee convergence nor do they guaranty satisfaction of Effector Bounds.\cite{ctrl_alloc_survey} The last point is especially detrimental for this application as we require that each thruster only produces a positive amount of Thrust.
	Instead we will define "finding the maximal force in a direction" as an optimization Problem in the framework of Linear Programming and solve it using the SciPy library.

	
	In the Linear Program every thruster in the Set will be represented using a six dimensional vector.
	\begin{align}
		\begin{pmatrix}
			d_x\\
			d_y\\
			d_z\\
			\tau_\alpha\\
			\tau_\beta\\
			\tau_\gamma
		\end{pmatrix}
	\end{align} 
	Where $d_x, d_y, d_z$ are the three translational force components of the Thruster and $\tau_\alpha, \tau_\beta, \tau_\gamma$ are the Torque components.
	By concatenating the vectors of the thrusters we are evaluating we get the control effectiveness matrix, which when multiplied by the thrust allocation vector $s$ results in the forces/torques resulting from the allocation.
	\begin{align}
		CEM \cdot s = \begin{pmatrix}
			{d   _1}_x      &  \hdots & {d   _i}_x      & \hdots & {d   _N}_x     \\ 
			{d   _1}_y      &  \hdots & {d   _i}_y      & \hdots & {d   _N}_y     \\
			{d   _1}_z      &  \hdots & {d   _i}_z      & \hdots & {d   _N}_z     \\
			{\tau_1}_\alpha &  \hdots & {\tau_i}_\alpha & \hdots & {\tau_N}_\alpha\\
			{\tau_1}_\beta  &  \hdots & {\tau_i}_\beta  & \hdots & {\tau_N}_\beta \\
			{\tau_1}_\gamma &  \hdots & {\tau_i}_\gamma & \hdots & {\tau_N}_\gamma
		\end{pmatrix}\cdot \begin{pmatrix}
			s_1\\\vdots\\s_i\\\vdots\\s_N
		\end{pmatrix} = \begin{pmatrix}
		F_x\\
		F_y\\
		F_z\\
		\tau_\alpha\\
		\tau_\beta\\
		\tau_\gamma
	\end{pmatrix}
	\end{align}
	
	The goal is to find a Thrust allocation that does not introduce Torques, $\implies \tau_\alpha = \tau_\beta = \tau_\gamma = 0$, and which results in a force in the direction of the point on the Unit Sphere $(1, \phi, \theta)$:
	\begin{align}
		F_x &= k \cdot \cos(\phi) \sin(\theta)\\
		F_y &= k \cdot \sin(\phi) \sin(\theta)\\
		F_z &= k \cdot \cos(\theta)
	\end{align}
	This results in the problem that we want to maximize $k$ such that:
	\begin{align}
		CEM \cdot s = k\cdot \begin{pmatrix}
			\cos(\phi) \sin(\theta)\\
			\sin(\phi) \sin(\theta)\\
			\cos(\theta)\\
			0\\
			0\\
			0
		\end{pmatrix}
	\end{align}
	Which we can bring into the form of a Linear Program:
	\begin{align}
		&\max k\\
		&\underbrace{\begin{pmatrix}
				{d   _1}_x      &  \hdots & {d   _i}_x      & \hdots & {d   _N}_x      & -\cos(\phi)\sin(\theta)\\
				{d   _1}_y      &  \hdots & {d   _i}_y      & \hdots & {d   _N}_y      & -\sin(\phi)\sin(\theta)\\
				{d   _1}_z      &  \hdots & {d   _i}_z      & \hdots & {d   _N}_z      & -\cos(\theta)\\
				{\tau_1}_\alpha &  \hdots & {\tau_i}_\alpha & \hdots & {\tau_N}_\alpha & - 0\\
				{\tau_1}_\beta  &  \hdots & {\tau_i}_\beta  & \hdots & {\tau_N}_\beta  & - 0\\
				{\tau_1}_\gamma &  \hdots & {\tau_i}_\gamma & \hdots & {\tau_N}_\gamma & - 0
		\end{pmatrix}}_{A} \cdot \underbrace{\begin{pmatrix}
				s_1\\\vdots\\s_i\\\vdots\\s_N\\ k
		\end{pmatrix}}_{x}
		= \begin{pmatrix}
			0\\0\\0\\0\\0\\0
		\end{pmatrix}\\
		&\text{subject to:}\notag\\
		&0 \leq s_i \leq f_i\\
		&0 \leq k
	\end{align}
	This can be solved using a standard LP-Solver.
	
\newpage	
	\section{Genetic Algorithm}
	For our Genetic Algorithm a Gene is a bit-string, where each bit corresponds to a thruster.
	We supply the Algorithm with a Big set of Thrusters, from which it will select a subset of Thrusters which produce the least error.
	\subsection{High Level Overview of the Algorithm}
		\begin{enumerate}
			\item Generate a starting population of Size $P$.
			\item Evaluate the entire population. \label{Evaluation}
				\subitem If the stopping criteria has been reached, terminate.
			\item Generate new Generation
				\subitem Top $P * p_{elite}$ are Elite and are kept 1:1
				\subitem $P * p_{crossover}$ genes are generated by two random parents from the elite.
				\subitem $P * p_{mutation}$ genes are generated by mutating an elite by flipping bits in the bit-string with probability $p_{ratio} * p_{radiation}$
				\subitem $P * p_{generation}$ genes are randomly generated by setting a bit in an empty bit string with probability $p_{ratio}$
			\item goto \ref{Evaluation}
		\end{enumerate}
		$p_{ratio}$ is the ratio of set bits in the best gene in the population. $p_{ratio} = \frac{\# \text{ number of set bits in best gene}}{\#\text{ of bits in gene}}$. This value changes during the runtime of the algorithm.
	\subsection{Gene Mapping to Thrusters}
		We supply the algorithm with a big (1000+) set of Thrusters distributed on the Body in some way. Each of those thrusters correspond to a bit in the bit string. If the bit of a thruster is set it gets included in the Control Effectiveness Matrix of the Force calculation.
	
	\subsection{Stopping Criteria}
		We implement two Stopping Criteria.
		The first stopping criteria is evaluated on a per direction basis. For each direction that gets evaluated does the Error in that direction fall below a certain threshold. If this is true for all directions for a gene, the algorithm terminates.
		
		
		The second is concerned with iteration count, in order to avoid infinite runtime when the problem cannot be solved with regards to the Error bounds given in the first Termination Criteria.
	\subsection{Crossover}
		We select two parents randomly from the elite population. 
		For each bit in the new gene we flip a fair coin  $p[\text{Parent 1}] = p[\text{Parent 2}]$ from which parent we copy that bit.
\newpage
	\section{Results}
	In the following Pages we are listing results from this Work.\\
	They are structured by first listing the Parameters to the Genetic Algorithm:
	\begin{enumerate}
		\item $p_{elite}$
		\item $p_{crossover}$
		\item $p_{mutation}$
		\item $p_{generation}$
		\item $p_{radiation}$
	\end{enumerate}
	Then Graphs visualizing the  $Force(C, \phi, \theta)$, $Target(\phi, \theta)$ and $Error(C, \phi, \theta)$ follow.
	At the end we visualize which Thrusters have been selected on the Asteroid using a 3D Render and show the Resulting Force Profile of the Selected Thrusters.
	\def\result#1#2#3
	{
		\newpage
		\subsection{#2}
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.65\textwidth]{"media/Optimization_#1/Asteroid.png"}
			\caption{#2 #3}
		\end{figure}
		\subsubsection{Genetic Algorithm Parameters}
		\begin{center}
			\input{"media/Optimization_#1/parameters.tex"}
			
		\end{center}
		\subsubsection{Error and $p_{ratio}$ history during runtime}
		\begin{center}
			\includegraphics[width=0.8\textwidth]{"media/Optimization_#1/thruster_configuration.py_history.png"}
		\end{center}
	
		\subsubsection{Performance in direction $(1, \phi, \theta)$}
		\begin{center}
			\includegraphics[width=0.8\textwidth]{"media/Optimization_#1/thruster_configuration.py_plot_magnitude.png"}
			\includegraphics[width=0.8\textwidth]{"media/Optimization_#1/thruster_configuration.py_plot_target.png"}
			\includegraphics[width=0.8\textwidth]{"media/Optimization_#1/thruster_configuration.py_plot_error.png"}
		\end{center}
	
		\subsubsection{Visualization of Selected Thrusters}
		\begin{center}
		
			\begin{figure}[H]
				\begin{subfigure}[b]{0.5\textwidth}
					\includegraphics[width=\textwidth]{"media/Optimization_#1/All_Thrusters.png"}
					\caption{Optimization #1 #2: All Thrusters}
					\label{fig:All Thrusters #1}
				\end{subfigure}
				\begin{subfigure}[b]{0.5\textwidth}
					\includegraphics[width=\textwidth]{"media/Optimization_#1/Selected_Thrusters.png"}
					\caption{Optimization #1 #2: Selected Thrusters}
					\label{fig:Selected Thrusters #1}
				\end{subfigure}
				\caption{Optimization #1 #2: Thruster Visualizations, Red Cylinders are representations of Thrusters}%
				\label{fig:Thruster_Vis_#1}%
			\end{figure}
		\end{center}
		\subsection{Force Profile of the Selected Thrusters}
		\begin{center}
			\begin{figure}[H]
				\begin{subfigure}[b]{0.5\textwidth}
					\includegraphics[width=\textwidth]{"media/Optimization_#1/Target.png"}
					\caption{Optimization #1 #2: Target Functions Force Profile as a Surface}
					\label{fig:Optimization #1 Target Surface}
				\end{subfigure}
				\begin{subfigure}[b]{0.5\textwidth}
					\includegraphics[width=\textwidth]{"media/Optimization_#1/Magnitude.png"}
					\caption{Optimization #1 #2: Selected Thrusters Force Profile}
					\label{fig:Optimization #1 Magnitude Surface}
				\end{subfigure}
				\caption{Optimization #1 #2: Force Profile of the Optimization}
			\end{figure}
		\end{center}
	}
	\result{1}{Asteroid Hygiea}{\cite{_urech_2011}}
	\result{2}{Asteroid Itokawa}{\cite{itokawa}}
	\result{3}{Asteroid Eros}{\cite{Gaskell_2021}}
	\result{4}{Asteroid Eros}{\cite{Gaskell_2021}}
\newpage
\listoffigures
\newpage
\bibliographystyle{plain}
\bibliography{draft}

\end{document}    
